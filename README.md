# Background Generation

This is the repository holding the code used to generate the toy background and data used in the RECAST tutorial.
It works with `atlas/analysisbase:21.2.85-centos7`.

## Reference location

The file generated is used in the RECAST for ATLAS tutorial and is currently located on EOS at

```
/eos/user/r/recasttu/ATLASRecast2021/external_data.root
```

If authenticated as `recasttu` the file can be accessed with `XRootD` by

```console
xrdcp root://eosuser.cern.ch//eos/user/r/recasttu/ATLASRecast2021/external_data.root .
```
